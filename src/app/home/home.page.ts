import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  bankList: any;
  items: any;
  selectedCity = 'BANGALORE';
  loading: any;
  searchTerm = '';

  constructor(public http: Http, public loadingCtrl: LoadingController) { }

  ngOnInit() {
    this.getBankList(this.selectedCity);
  }

  onSelectChange(city: any) {
    this.getBankList(city.detail.value);
  }


  getBankList(city: any) {
    this.presentLoading('Getting Bank List');
    this.http.get('https://vast-shore-74260.herokuapp.com/banks?city=' + city, null)
      .subscribe(
        data => {
          this.bankList = data.json();
          this.initializeItems();
          this.stopLoading();
        }
      );
  }

  async presentLoading(content: string) {
    this.loading = await this.loadingCtrl.create({
      message: content,
      duration: 2000
    });
    await this.loading.present();

    const { role, data } = await this.loading.onDidDismiss();
  }

  stopLoading() {
    console.log(this.loading);
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  initializeItems() {
    this.items = this.bankList;
  }

  filterBanks(ev: any) {
    // Reset items back to all of the banks
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.bank_name.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.ifsc.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.branch.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

}
